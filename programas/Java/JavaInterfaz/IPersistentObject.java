
public interface IPersistentObject {
  public void find();
  public void save();
  public void delete();
  public void retrieve();
}
