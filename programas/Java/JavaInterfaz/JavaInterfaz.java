
public class JavaInterfaz {
  public static void main(String[] args) {
    User user = new Player();

    //Llamando los metodos de la clase User
    user.setId(1);
    user.setName("Pepe");

    IPersistentObject o = (IPersistentObject) user;
    //Se puede llamar desde el objeto o todos los metodos de la interfaz IPersistentObject
    o.find();
    o.save();
    o.delete();
    o.retrieve();

    Player p = (Player) user;
    p.run();
    p.jump();
    p.slice();
    p.attack();

    System.out.println(user.getName());
  }
}
