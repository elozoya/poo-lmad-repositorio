

public class Player extends User implements IPersistentObject {
  private int life;
  private int stamina;
  private int defense;

  public void run() {
    System.out.println("Run");
  }

  public void jump() {
    System.out.println("Jump");
  }

  public void slice() {
    System.out.println("Slice");
  }

  public void attack() {
    System.out.println("Attack");
  }

  public void find() {
    System.out.println("Implementing find");
  }

  public void save() {
    System.out.println("Implementing save");
  }

  public void delete() {
    System.out.println("Implementing delete");
  }

  public void retrieve() {
    System.out.println("Implementing retrieve");
  }
}
