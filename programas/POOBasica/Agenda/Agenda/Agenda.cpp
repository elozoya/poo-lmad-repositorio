#include "Agenda.h"
#include <algorithm>

void Agenda::addUser(User user) {
	this->users.push_back(user);
}

vector<User> Agenda::getUsers() {
	return this->users;
}

void Agenda::sortByName() {
	std::sort(users.begin(), users.end(), compareUsersByName);
}

void Agenda::sortByPower() {
	std::sort(users.begin(), users.end(), compareUsersByPower);
}

void Agenda::reverse() {
	std::reverse(users.begin(), users.end());
}

User Agenda::findByName(string name) {
	for (int i = 0; i < users.size(); i++) {
		if (users[i].getName() == name) {
			return users[i];
		}
	}
	return User();
}

bool Agenda::compareUsersByName(User& first, User& second) {
	return first.getName() > second.getName();
}

bool Agenda::compareUsersByPower(User& first, User& second) {
	return first.getPower() > second.getPower();
}