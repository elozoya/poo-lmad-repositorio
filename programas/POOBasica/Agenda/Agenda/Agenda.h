#pragma once
#include <vector>
#include "User.h"
using namespace std;

class Agenda {
public:
	void addUser(User user);
	vector<User> getUsers();
	void sortByName();
	void sortByPower();
	void reverse();
	User findByName(string name);
	static bool compareUsersByName(User& first, User& second);
	static bool compareUsersByPower(User& first, User& second);
private:
	vector<User> users;
};