#include "User.h"

User::User(string name, int power) {
	this->setName(name);
	this->setPower(power);
}

string User::getName() {
	return this->name;
}

void User::setName(string name) {
	this->name = name;
}

int User::getPower() {
	return this->power;
}

void User::setPower(int power) {
	this->power = power;
	this->power = (this->power > 10) ? 10 : this->power;
	this->power = (this->power < 0) ? 0 : this->power;
}