#pragma once
#include <string>
using namespace std;

class User {
public:
	User(string name = "", int power = 0);
	string getName();
	void setName(string name);
	int getPower();
	void setPower(int power);
private:
	string name;
	int power;
};