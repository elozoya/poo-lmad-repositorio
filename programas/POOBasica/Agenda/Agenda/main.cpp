#include <iostream>
#include <vector>
#include "Agenda.h"
#include "User.h"
using namespace std;

class AgendaUtil {
public:
	static void printUser(User& user) {
		cout << "Name: " << user.getName() << " ";
		cout << "Power: " << user.getPower();
		cout << endl;
	}

	static void printAgenda(Agenda& agenda) {
		vector<User> users = agenda.getUsers();
		for (int i = 0; i < users.size(); i++) {
			AgendaUtil::printUser(users[i]);
		}
	}
};

int main(int argc, char** argv) {
	Agenda agenda;
	User user1 = User("Emma", 5);
	User user2 = User("Jordan", 9);
	User user3 = User("Sir OOP", 20);

	agenda.addUser(user1);
	agenda.addUser(user2);
	agenda.addUser(user3);

	AgendaUtil::printAgenda(agenda);
	agenda.sortByName();
	AgendaUtil::printAgenda(agenda);
	agenda.sortByPower();
	agenda.reverse();
	AgendaUtil::printAgenda(agenda);

	AgendaUtil::printUser(agenda.findByName("Emm"));
	AgendaUtil::printUser(agenda.findByName("Emma"));
	cin.get();
	return 0;
}