#include "CLIProgram.h"
#include <iostream>

CLIProgram::CLIProgram() {
	std::cout << "Constructor" << std::endl;
}

CLIProgram::~CLIProgram() {
	std::cout << "Destructor" << std::endl;
}

void CLIProgram::run(void) {
	message = "Hey!";
	std::cout << this->message << std::endl;
	this->exitCode = 0;
}