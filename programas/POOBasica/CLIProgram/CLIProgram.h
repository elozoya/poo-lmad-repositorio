#pragma once
#include <string>
using std::string;

class CLIProgram {
public:
	CLIProgram();
	~CLIProgram();
	void run(void);
private:
	string message;
protected:
	int exitCode;
};