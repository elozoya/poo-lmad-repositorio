#include <iostream>
#include "CLIProgram.h"

using namespace std;

int main(int argc, char** argv) {
	CLIProgram p;
	p.run();
	cin.get();
	return 0;
}