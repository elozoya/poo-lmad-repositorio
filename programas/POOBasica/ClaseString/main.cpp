#include <iostream>
#include <string>
using namespace std;

void ejemploCapacidad(void);
void ejemploAcceso(void);
void ejemploModificadores(void);
void ejemploOperaciones(void);
void ejemploIteradores(void);

int main(int argc, char** argv) {
	ejemploCapacidad();
	ejemploAcceso();
	ejemploModificadores();
	ejemploOperaciones();
	ejemploIteradores();
	cin.get();
	return 0;
}

void ejemploCapacidad(void) {
	string msg1 = "Ejemplo capacidad";
	cout << "Mensaje: " << msg1 << endl;
	cout << "Size:    " << msg1.size() << endl;

	if (msg1.empty()) {
		cout << "Mensaje vacio" << endl;
	} else {
		cout << "El mensaje no esta vacio" << endl;
	}

	msg1.clear();
	cout << (msg1.empty() ? "Mensaje vacio" : "El mensaje no esta vacio") << endl;
}

void ejemploAcceso(void) {
	string str ("Ejemplo de acceso");

	//Iterando mediante el metodo at()
	for (int i = 0; i < str.length(); i++) {
		cout << str.at(i);
	}
	cout << endl;

	//Iterando mediante la sobrecarga del operador []
	for (int i = 0; i < str.length(); i++) {
		cout << str[i];
	}
	cout << endl;

}

void ejemploModificadores(void) {
	string msg = "Ejemplo";
	msg.append(" Mod");
	msg += "ificadore";
	msg.push_back('s');

	cout << msg << endl;
}

void ejemploOperaciones(void) {
	string msg1 = "Ejemplo Operaciones";
	string msg2 = "Test";
	if (msg1 > msg2) {
		cout << "'" << msg1 << "'" << " es mayor que " << "'" << msg2 << "'" << endl;
	} else {
		cout << "'" << msg1 << "'" << " no es mayor que " << "'" << msg2 << "'" << endl;
	}

}

void ejemploIteradores(void) {
	string str ("Ejemplo de Iteradores");
	for (string::iterator it = str.begin(); it != str.end(); it++) {
		cout << *it;
	}
	cout << endl;

	//Reverse Iterator
	for (string::reverse_iterator it = str.rbegin(); it != str.rend(); it++) {
		cout << *it;
	}
	cout << endl;

}