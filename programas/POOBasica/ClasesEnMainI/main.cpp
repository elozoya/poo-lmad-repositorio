//Clases en main
#include <iostream>
using namespace std;

class Vector3 {
public:
	//Constructores
	Vector3() {
		x = 0;
		y = 0;
		z = 0;
	}
	Vector3(float x, float y, float z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}

	//Getters and Setters
	float getX() {
		return x;
	}
	void setX(float x) {
		this->x = x;
	}
	float getY() {
		return y;
	}
	void setY(float y) {
		this->y = y;
	}
	float getZ() {
		return z;
	}
	void setZ(float z) {
		this->z = z;
	}

private:
	float x;
	float y;
	float z;
};


int main(int argc, char** argv) {
	Vector3 v(2, 3, 4);
	cout << "Clases en main I" << endl;
	cout << "Vector";
	cout << "<" << v.getX() << ", " << v.getY() << ", " << v.getZ() << ">" << endl;
	cin.get();
	return 0;
}