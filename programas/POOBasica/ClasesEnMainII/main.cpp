//Clases en main
#include <iostream>
using namespace std;

class Vector3 {
public:
	//Constructores
	Vector3();
	Vector3(float x, float y, float z);

	//Getters and Setters
	float getX();
	void setX(float x);
	float getY();
	void setY(float y);
	float getZ();
	void setZ(float z);

private:
	float x;
	float y;
	float z;
};

Vector3::Vector3() : x(0), y(0), z(0) {
}

Vector3::Vector3(float x, float y, float z) : x(x), y(y), z(z) {
}

float Vector3::getX() {
	return x;
}

void Vector3::setX(float x) {
	this->x = x;
}

float Vector3::getY() {
	return y;
}

void Vector3::setY(float y) {
	this->y = y;
}

float Vector3::getZ() {
	return this->z;
}

void Vector3::setZ(float z) {
	this->z = z;
}

int main(int argc, char** argv) {
	Vector3 v(2, 3, 4);
	cout << "Clases en main II" << endl;
	cout << "Vector";
	cout << "<" << v.getX() << ", " << v.getY() << ", " << v.getZ() << ">" << endl;
	cin.get();
	return 0;
}