#pragma once
#include "Vector3.h"

class Model {
public:
	Model() {
		position = Vector3(0, 0, 0);
	}

	Model(Vector3 position) {
		this->setPosition(position);
	}

	Vector3 getPosition() {
		return position;
	}

	void setPosition(Vector3 position) {
		this->position = position;
	}

private:
	Vector3 position;
};