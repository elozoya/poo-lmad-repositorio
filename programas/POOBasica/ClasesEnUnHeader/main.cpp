//Author: Emmanuel Lozoya
#include <iostream>
#include "Vector3.h"
#include "Model.h"

using namespace std;

int main(int argc, char** argv) {
	Vector3 v(2, 3, 4);
	Model m;
	m.setPosition(Vector3(5, 6, 7));
	cout << "Vector";
	cout << "<" << v.getX() << ", " << v.getY() << ", " << v.getZ() << ">" << endl;
	cout << "Model position";
	cout << "<" << m.getPosition().getX() << ", " << m.getPosition().getY() << ", " << m.getPosition().getZ() << ">" << endl;
	cin.get();
	return 0;
}