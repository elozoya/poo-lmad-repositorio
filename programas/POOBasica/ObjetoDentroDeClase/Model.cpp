#include "Model.h"

Model::Model() : position() {
}

Model::Model(Vector3 position) : position(position.getX(), position.getY(), position.getZ()) {
}

Vector3 Model::getPosition() {
	return position;
}

void Model::setPosition(Vector3 position) {
	this->position = position;
}