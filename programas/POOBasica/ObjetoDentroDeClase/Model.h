#ifndef _MODEL_H_
#define _MODEL_H_

#include "Vector3.h"

class Model {
public:
	Model();
	Model(Vector3 position);

	Vector3 getPosition();
	void setPosition(Vector3 position);

private:
	Vector3 position;
};
#endif