#include "Vector3.h"

Vector3::Vector3() : x(0), y(0), z(0) {
}

Vector3::Vector3(float x, float y, float z) : x(x), y(y), z(z) {
}

float Vector3::getX() {
	return x;
}

void Vector3::setX(float x) {
	this->x = x;
}

float Vector3::getY() {
	return y;
}

void Vector3::setY(float y) {
	this->y = y;
}

float Vector3::getZ() {
	return this->z;
}

void Vector3::setZ(float z) {
	this->z = z;
}

std::ostream& operator<<(std::ostream& os, Vector3& v) {
	std::cout << "<" << v.getX() << ", " << v.getY() << ", " << v.getZ() << ">";
	return os;
}