#ifndef _VECTOR3_H_
#define _VECTOR3_H_

#include <iostream>

class Vector3 {
public:
	//Constructores
	Vector3();
	Vector3(float x, float y, float z);

	//Getters and Setters
	float getX();
	void setX(float x);
	float getY();
	void setY(float y);
	float getZ();
	void setZ(float z);

	//Sobrecarda de operadores
	friend std::ostream& operator<<(std::ostream& os, Vector3& v);

private:
	float x;
	float y;
	float z;
};

#endif