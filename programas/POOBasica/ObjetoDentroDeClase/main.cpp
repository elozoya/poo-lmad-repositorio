//Author: Emmanuel Lozoya
//Objetivos:
//*****Mostrar un ejemplo de Composicion/Composition
//*****Mostrar un ejemplo de un objeto dentro de una clase
//*****Explicar el error de doble inclusion 
//*****Su solucion con el #pragma once o su version con #ifndef #define #endif

//Objetivos extras:
//Mostrar una sobrecarga del operador

#include <iostream>
#include "Vector3.h"
#include "Model.h"

using namespace std;


int main(int argc, char** argv) {
	Vector3 v(2, 3, 4);
	Model m;
	m.setPosition(Vector3(5, 6, 7));
	cout << "Vector";
	cout << "<" << v.getX() << ", " << v.getY() << ", " << v.getZ() << ">" << endl;
	cout << "Model position";
	cout << "<" << m.getPosition().getX() << ", " << m.getPosition().getY() << ", " << m.getPosition().getZ() << ">" << endl;

	//Sobrecarga de operadores
	cout << endl;
	cout << "Version con sobrecarga del operador <<" << endl;
	cout << "Vector";
	cout << v << endl;
	cout << "Model position";
	cout << m.getPosition() << endl;
	cin.get();
	return 0;
}