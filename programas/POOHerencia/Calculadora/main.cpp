//Autor: Emmanuel Lozoya
//Ejemplo de Herencia
//Objetivo
//Agregar nueva funcionalidad

#include <iostream>
using namespace std;
#define PI 3.14159265

class CalculadoraBasica {
public:
	int sumar(int a, int b) {
		return a + b;
	}
};

class CalculadoraCientifica : public CalculadoraBasica {
public:
	//Returns the cosine of an angle of x radians
	double coseno(double x) {
		return cos(x);
	}
};

int main(int argc, char** argv) {
	CalculadoraBasica basica;
	CalculadoraCientifica cientifica;
	cout << "Ejemplo de Herencia" << endl;
	cout << "Objetivo: Agregar nuevas funcionalidades" << endl << endl;
	cout << "Proyecto: Calculadora Basica y Calculadora Cientifica" << endl;
	cout << "5 + 10 = " << basica.sumar(5, 10) << endl;
	cout << "4 + 30 = " << cientifica.sumar(4, 30) << endl;
	cout << "coseno de PI = " << cientifica.coseno(PI) << endl;
	cin.get();
	return 0;
}