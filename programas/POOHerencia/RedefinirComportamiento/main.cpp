//Autor: Emmanuel Lozoya
//Ejemplo de Herencia
//Objetivo
//Redefinir funcionalidad

#include <iostream>
using namespace std;

class GreeterBasic {
public:
	void sayHello() {
		cout << "GreeterBasic says hello" << endl;
	}
};

class GreeterSpecial : public GreeterBasic {
public:
	void sayHello() {
		cout << "GreeterSpecial says hello and also call GreeterBasic sayHello" << endl;
		GreeterBasic::sayHello();
	}
};

int main(int argc, char** argv) {
	cout << "Ejemplo de Herencia" << endl;
	cout << "Objetivo: Redefinir comportamiento" << endl << endl;
	GreeterBasic greeterBasic;
	GreeterSpecial greeterSpecial;

	greeterBasic.sayHello();
	cout << "----------------------------------------" << endl;
	greeterSpecial.sayHello();
	cin.get();
	return 0;
}