#include <iostream>
using namespace std;

class Animal {
public:
	void eat() {
		cout << "Animal eat" << endl;
	}
};

class Mammal : public Animal {
public:
	void breathe() {
		cout << "Mammal breathe" << endl;
	}
};

class WingedAnimal : public Animal {
public:
	void flap() {
		cout << "WingedAnimal flap" << endl;
	}
};

class Bat : public Mammal, public WingedAnimal {
public:
	void foo() {
		cout << "Bat foo" << endl;
	}
};

int main(int argc, char** argv) {
	Bat batman;
	batman.breathe();
	batman.flap();
	batman.foo();
	//Error
	//Bat::eat es ambiguo
	//batman.eat();
	//Para poder mandar a llamar el metodo eat
	//se necesita hacer lo siguiente
	Mammal& mammal = batman;
	WingedAnimal& winged = batman;
	mammal.eat();
	winged.eat();

	cin.get();
	return 0;
}