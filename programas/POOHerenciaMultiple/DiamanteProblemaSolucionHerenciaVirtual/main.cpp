#include <iostream>
using namespace std;

class Animal {
public:
	void eat() {
		cout << "Animal eat" << endl;
	}
};

class Mammal : public virtual Animal {
public:
	void breathe() {
		cout << "Mammal breathe" << endl;
	}
};

class WingedAnimal : public virtual Animal {
public:
	void flap() {
		cout << "WingedAnimal flap" << endl;
	}
};

class Bat : public Mammal, public WingedAnimal {
public:
	void foo() {
		cout << "Bat foo" << endl;
	}
};

int main(int argc, char** argv) {
	Bat batman;
	batman.breathe();
	batman.flap();
	batman.foo();
	batman.eat();
	cin.get();
	return 0;
}