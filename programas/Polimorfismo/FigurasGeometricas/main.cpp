#include <iostream>
using namespace std;

class Shape {
protected:
	double width;
	double height;
public:
	Shape(double w = 0, double h = 0) : width(w), height(h) {}

	//Definir un metodo que puede tener muchas formas
	virtual double getArea() = 0;
};

class Rectangle : public Shape {
public:
	Rectangle(double w = 0, double h = 0) : Shape(w, h) {}

	//Sobrescriendo. Creando una forma
	double getArea() {
		return width * height;
	}
};

class Triangle : public Shape {
	public:
	Triangle(double w = 0, double h = 0) : Shape(w, h) {}

	//Sobrescribiendo. Creando otra forma
	double getArea() {
		return (width * height) / 2;
	}
};

int main(int argc, char** argv) {
	Rectangle rectangle = Rectangle(100, 2);
	Triangle triangle = Triangle(50, 10);

	cout << "Directamente desde el objeto" << endl;
	cout << "Rectangle: " << rectangle.getArea() << endl;
	cout << "Triangle:  " << triangle.getArea() << endl;

	Shape* shapePtr1 = &rectangle;
	Shape* shapePtr2 = &triangle;

	cout << "Llamar desde un puntero de la clase base" << endl;
	cout << "ShapePtr1(Rectangle): " << shapePtr1->getArea() << endl;
	cout << "ShapePtr2(Triangle):  " << shapePtr2->getArea() << endl;

	Shape* shape1 = new Rectangle(100, 3);
	Shape* shape2 = new Triangle(10, 10);
	cout << "Llamar desde un puntero de la clase base con memoria dinamica" << endl;
	cout << "Shape1(Rectangle): " << shape1->getArea() << endl;
	cout << "Shape2(Triangle):  " << shape2->getArea() << endl;
	delete shape1;
	delete shape2;

	cin.get();
	return 0;
}