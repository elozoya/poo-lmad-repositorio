//Ejemplo: Enumeraciones
//Autor: Emmanuel Lozoya
#include <iostream>
using namespace std;

enum color {Red, Green, Crimson = 10, DarkSlateGrey};
//enum foo {bar, Green}; //error: Redefinicion de Green

//C++11 Enum Class
//enum class GameStatus {
//     Paused = 1,
//     Playing = 2
//};

int main(int argc, char** argv) {
  cout << "Red: " << Red << " Green: " << Green << " Crimson: " << Crimson << " DarskSlateGrey: " << DarkSlateGrey << endl;
  color z;
  z = Crimson;
  if (z != Red) {
	  cout << "Color z no es rojo" << endl;
  }
  //C++11
  //GameStatus gameStatus = GameStatus::Playing;
  //if (gameStatus == GameStatus::Playing) {
  //  cout << "Dibujar nivel" << endl;
  //}

  //gameStatus = GameStatus::Paused;
  //if (gameStatus == GameStatus::Paused) {
  //  cout << "Dibujar la pantalla de pause" << endl;
  //}
  //cout << "Para imprimir el valor de un EnumClass hay que utilizar static_cast<T>()" << endl;
  //cout << "GameStatus::Paused: " << static_cast<int>(GameStatus::Paused) << endl;

  cin.get();
  return 0;
}