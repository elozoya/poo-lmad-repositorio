#include <iostream>
using namespace std;

struct Persona {
	bool cabelloLimpio;
	void ensuciar() {
		cabelloLimpio = false;
	}
};

struct Shampoo {
	bool agitado;
	void agitar() {
		agitado = true;
	}
	void reposar() {
		agitado = false;
	}
	void limpiar(Persona &persona) {
		if (agitado == false) {
			return;
		}
		persona.cabelloLimpio = true;
		agitado = false;
	}
};

Persona crearPersonaConCabelloSucio() {
	Persona p;
	p.cabelloLimpio = false;
	return p;
}
void agitarShampoo(Shampoo &shampoo) {
	shampoo.agitado = true;
}
void limpiarPersona(Shampoo &shampoo, Persona &persona) {
	if (shampoo.agitado == false) {
		return;
	}
	persona.cabelloLimpio = true;
	shampoo.agitado = false;
}
Shampoo crearShampoo() {
	Shampoo shampoo;
	shampoo.agitado = false;
	return shampoo;
}

int main(int argc, char** argv) {
	Persona persona;
	Shampoo shampoo;

	//Version 1
	//Funciones fuera de la estructura
	//persona = crearPersonaConCabelloSucio();
	//agitarShampoo(shampoo);
	//limpiarPersona(shampoo, persona);

	//Version 2
	//Funciones dentro de la estructura
	persona.ensuciar();
	shampoo.reposar();
	shampoo.agitar();
	shampoo.limpiar(persona);

	if (persona.cabelloLimpio) {
		cout << "Cabello limpio C:" << endl;
	} else {
		cout << "Cabello no limpio :(" << endl;
	}

	cin.get();
	return 0;
}