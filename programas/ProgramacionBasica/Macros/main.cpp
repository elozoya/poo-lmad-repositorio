//Ejemplo: Directivas de Preprocesador
//Autor: Emmanuel Lozoya

//Directiva de Inclusion
#include <iostream>

//Macro Constants
#define MENSAJE "Hola mundo!"

//Macro Function
#define INCREMENTAR(x) x+1
#define MAXIMO(a, b) ((a) > (b) ? (a) : (b))

//Funcion para comparar con el macro MAXIMO
int max(int a, int b) {
	return a > b ? a : b;
	//La linea de arriba es equivalente a las siguientes lineas
	//if (a > b) {
	//	return a;
	//} else {
	//	return b;
	//}
}

int main(int argc, char** argv) {
	std::cout << MENSAJE << std::endl;
	std::cout << INCREMENTAR(7) << std::endl;
	std::cout << "El numero mayor entre 2 y 5 es: " << MAXIMO(2, 5) << std::endl;
	std::cout << "El numero mayor entre 3 y 6 es: " << max(3, 6) << std::endl;
	std::cout << "Archivo: " << __FILE__ << " Linea: " << __LINE__ << std::endl;
	std::cout << "El proceso de compilacion inicio: " << __DATE__ << " " << __TIME__ << std::endl;;
	std::cin.get();
	return 0;
}