//Ejemplo: Pasar por referencia
//Autor: Emmanuel Lozoya
#include <iostream>
using namespace std;

//Pasar por Valor/Copia
void addOneVersion1(int a) {
	a++;
}

//Pasar por Referencia
void addOneVersion2(int &a) {
	a++;
}

//Pasar por Direccion/Puntero
void addOneVersion3(int *a) {
	*a = *a + 1;
}

//Pasar por Const Reference
void suma(const int &x, const int &y, int &resultado) {
	//El valor de 'x' y 'y' se pueden usar para realizar calculos
	//pero no se pueden modificar
	//x = 5; //Marca error al compilar
	resultado = x + y;
}

int main(int argc, char** argv) {
	int z = 5;
	cout << "Valor original de z: " << z << endl;
	addOneVersion1(z);
	cout << "Add One version 1: " << z << endl;
	addOneVersion2(z);
	cout << "Add One version 2: " << z << endl;
	addOneVersion3(&z);
	cout << "Add One version 3: " << z << endl;
	int a = 2;
	int b = 3;
	int c = 0;
	suma(a, b, c);
	cout << "a + b = " << c << endl;
	cin.get();
	return 0;
}