#include <iostream>
using namespace std;

/*Command Line Arguments*/
/*The variables are named by convention:
	argc (argument count)
	argv (argument vector)
but they can be given any valid name.
*/
int main(int argc, char** argv) {
	cout << "Command Line Arguments Example" << endl;
	for (int i = 0; i < argc; i++) {
		cout << argv[i] << endl;
	}
	cin.get();
	return 0;
}