#include "Random.h"
#include <ctime>    // For time()
#include <cstdlib>  // For srand() and rand()

Random::Random() : initialized(false) {
}

int Random::getRandom() {
	init();
	return rand();
}

int Random::getRandom(int min, int max) {
	init();
	return rand() % (max - min + 1) + min;
}

void Random::init() {
	if (!initialized) {
		initialized = true;
		srand(time(0));  // Initialize random number generator.
	}
}