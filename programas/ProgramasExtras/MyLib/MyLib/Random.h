#pragma once

class Random {
public:
	Random();
	int getRandom(void);
	int getRandom(int min, int max);
private:
	bool initialized;
	void init();
};