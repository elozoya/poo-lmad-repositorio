#ifndef _GRR
#define _GRR

#include <windows.h>
#include <windowsx.h>
#include <gl\GLU.h>
#include <gl\GL.h>
#include "glext.h"
#include "SkyDome.h"
#include "Terreno.h"
#include "VectorRR.h"
#include "Billboard.h"

class Camara: public VectorRR
{
public:
	VectorRR posc, dirc;

	Camara()
	{
	}	

	void CamaraUpdate()
	{
		gluLookAt(posc.X, posc.Y, posc.Z,
			posc.X + dirc.X, posc.Y + dirc.Y, posc.Z + dirc.Z, 
			0, 1, 0);
	}

	void CamaraAvanza(float vel)
	{
		posc.X = posc.X + dirc.X * vel;
		posc.Y = posc.Y + dirc.Y * vel;
		posc.Z = posc.Z + dirc.Z * vel;

		gluLookAt(posc.X, posc.Y, posc.Z,
			posc.X + dirc.X, posc.Y, posc.Z + dirc.Z, 
			0, 1, 0);
	}

	void CamaraGiraY(float grados)
	{
		Transforma(dirc, grados, Ejes::EjeY);
	}
};

class GraphRR: public Camara
{
public:
	SkyDome *sky;
	Terreno *terreno;
	Billboard *billBoard[10];	

	float angulo;

	GraphRR(HWND hWnd)
	{
		posc=VectorRR(0, 13, 10);
		dirc=VectorRR(0, 0, -1);
		float matAmbient[] = {1,1,1,1};
		float matDiff[] = {1,1,1,1};
		angulo=0;

		glShadeModel(GL_SMOOTH);
		//habilitamos el control de profundidad en el render
		glEnable(GL_DEPTH_TEST);
		//habilitamos la iluminacion
		glEnable(GL_LIGHTING);
		glMaterialfv(GL_FRONT, GL_AMBIENT, matAmbient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, matDiff);

		

		// habilitamos la luz 0 o primer luz
		glEnable(GL_LIGHT0);
		//habilitamos la forma de reflejar la luz
		glEnable(GL_COLOR_MATERIAL);
		glColorMaterial(GL_FRONT, GL_DIFFUSE);
		glEnable(GL_NORMALIZE);
		//glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR);
		//creamos el objeto skydome
		sky = new SkyDome(hWnd, 32,32,20, L"earth.jpg");		
		terreno = new Terreno(hWnd, L"terreno.jpg", L"texterr2.jpg", 400, 400);
		
		inicializaBillboards(billBoard, hWnd);	
	}

	void inicializaBillboards(Billboard *bills[], HWND hWnd)
	{
		float ye=terreno->Superficie(0,0);
		bills[0] = new Billboard(hWnd, L"Arbol.png", 6, 6, 0,ye-1,0);

		ye=terreno->Superficie(5,-5);
		bills[1] = new Billboard(hWnd, L"Arbol2.png", 6, 6, 5,ye-1,-5);

		ye=terreno->Superficie(-9,-15);
		bills[2] = new Billboard(hWnd, L"Arbol3.png", 8, 8, -9,ye-1,-15);
	}

	//el metodo render toma el dispositivo sobre el cual va a dibujar
	//y hace su tarea ya conocida
	void Render(HDC hDC)
	{
		//borramos el biffer de color y el z para el control de profundidad a la 
		//hora del render a nivel pixel.
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		glClearColor(0,0,0,0);
		glLoadIdentity();
		posc.Y = terreno->Superficie(posc.X, posc.Z) + 1.7;	
		CamaraUpdate();


		GLfloat LightAmb2[] = { 1, 1, 1, 1.0 };
		glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmb2);
		glPushMatrix();		
		glTranslatef(posc.X, posc.Y - 5, posc.Z);		
		//decimos que dibuje la media esfera
		sky->Draw();		
		glPopMatrix();

		LightAmb2[0] = 0.2; LightAmb2[1] = 0.2; LightAmb2[2] = 0.2; LightAmb2[3] = 0.0;
		glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmb2);

		GLfloat LightPos[] = {0.0, 60.0,0.0, 1.0 };	  
	    glLightfv(GL_LIGHT0, GL_POSITION, LightPos);
		terreno->Draw();
			
		billBoard[0]->Draw(posc);
		billBoard[1]->Draw(posc);
		billBoard[2]->Draw(posc);
		SwapBuffers(hDC);
	}

};

#endif 