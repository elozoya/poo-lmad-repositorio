#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>

using namespace std;

class Student {
	string name;
public:
	Student() {}
	Student(string name) : name(name) {}
	void setName(string name) {
		this->name = name;
	}
	string getName() {
		return this->name;
	}
	friend bool operator< (Student &a, Student &b);
};
bool operator< (Student &a, Student &b) {
	return a.getName() < b.getName();
}

int main(int argc, char** argv) {
	//STL Vector
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	cout << "v size = " << v.size() << endl;
	v.insert(v.end(), 5, 100);
	cout << "v size = " << v.size() << endl;

	for(int i = 0; i < v.size(); i++) {
		cout << v[i] << endl;
	}

	//STL List
	list<Student> students;
	students.push_back(Student("Emmanuel"));
	students.push_back(Student("Jordan"));
	students.push_front(Student("Steve Jobs"));
	students.push_front(Student("Tony Hawk"));

	students.sort(); //Se necesita sobrecargar el operador <
	list<Student> studentsReverse = students;
	studentsReverse.reverse();

	cout << "===============Students" << endl;
	for(list<Student>::iterator it = students.begin(); it != students.end(); it++) {
		cout << it->getName() << endl;
	}
	cout << "===============Students Reverse" << endl;
	for(list<Student>::iterator it = studentsReverse.begin(); it != studentsReverse.end(); it++) {
		cout << it->getName() << endl;
	}

	//STL Map
	map<string, string> foo;
	foo["color"] = "red";
	foo["width"] = "100px";
	foo["height"] = "200px";
	foo["map"] = "Maps are associative containers that store elements formed by a combination of a key value and a mapped value";

	cout << "Map size = " << foo.size() << endl;
	cout << "map definition: " << foo["map"] << endl;

	map<string, string>::iterator it = foo.find("color");
	if (it != foo.end()) {
		cout << "color: " << it->second << endl;
		foo.erase(it);
	}
	cin.get();
	return 0;
}