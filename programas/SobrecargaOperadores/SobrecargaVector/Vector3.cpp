#include "Vector3.h"

Vector3::Vector3() {
	x = 0;
	y = 0;
	z = 0;
}

Vector3::Vector3(float x, float y, float z) {
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector3 Vector3::Add(Vector3& a, Vector3& b) {
	Vector3 r;
	r.x = a.x + b.x;
	r.y = a.y + b.y;
	r.z = a.z + b.z;
	return r;
}

Vector3 Vector3::Subtract(Vector3& a, Vector3& b) {
	Vector3 r;
	r.x = a.x - b.x;
	r.y = a.y - b.y;
	r.z = a.z - b.z;
	return r;
}

Vector3 operator+(Vector3& a, Vector3& b) {
	return Vector3::Add(a, b);
}

Vector3 operator-(Vector3& a, Vector3& b) {
	return Vector3::Subtract(a, b);
}

ostream& operator<< (ostream &out, Vector3 &v) {
	out << "<" << v.x << "," << v.y << "," << v.z << ">";
	return out;
}