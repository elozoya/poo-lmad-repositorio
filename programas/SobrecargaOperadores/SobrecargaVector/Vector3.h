#pragma once
#include <iostream>
using namespace std;

class Vector3 {
public:
	float x, y, z;
	Vector3();
	Vector3(float x, float y, float z);

	static Vector3 Add(Vector3& a, Vector3& b);
	static Vector3 Subtract(Vector3& a, Vector3& b);
	friend Vector3 operator+(Vector3& a, Vector3& b);
	friend Vector3 operator-(Vector3& a, Vector3& b);
	friend ostream& operator<< (ostream &out, Vector3 &v);
};