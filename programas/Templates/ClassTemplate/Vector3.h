#pragma once
#include <iostream>
using namespace std;

class Vector3 {
	float x, y, z;
public:
	Vector3() : x(0), y(0), z(0) {}
	Vector3(float n) : x(n), y(n), z(n) {}
	Vector3(float x, float y, float z) : x(x), y(y), z(z) {}
	Vector3 operator+(const Vector3& other);
	friend ostream &operator<<(ostream &out, Vector3 v);
};
Vector3 Vector3::operator+(const Vector3&  other) {
	return Vector3( this->x + other.x, this->y + other.y, this->z + other.z);
}
ostream &operator<<(ostream &out, Vector3 v) {
	out << "<";
	out << v.x <<", ";
	out << v.y <<", ";
	out << v.z <<">";
    return out;
}