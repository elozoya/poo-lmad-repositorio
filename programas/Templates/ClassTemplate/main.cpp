#include <iostream>
#include <string>
#include "Vector3.h"
using namespace std;

template <class T>
class Pair {
	T a, b;
public:
	Pair() : a(), b() {}
	Pair(T first, T second) : a(first), b(second) {}

	T getA() {
		return a;
	}

	T getB() {
		return b;
	}

	void setA(T first) {
		a = first;
	}

	void setB(T second) {
		b = second;
	}
	friend ostream &operator<<(ostream &out, Pair<T> p) {
		out << "<";
		out << p.a <<", ";
		out << p.b <<">";
		return out;
	}
};

int main(int argc, char** argv) {
	Pair<int> p1 = Pair<int>(2, 3);
	Pair<float> p2 = Pair<float>(2.5, 3.5);
	Pair<double> p3 = Pair<double>(2.2, 3.2);
	Pair<string> p4 = Pair<string>("hey", "wahoo");
	Pair<Vector3> p5 = Pair<Vector3>(Vector3(3), Vector3(7));
	cout << p1 << endl;
	cout << p2 << endl;
	cout << p3 << endl;
	cout << p4 << endl;
	cout << p5 << endl;
	cin.get();
	return 0;
}