#include <iostream>
#include <string>
using namespace std;

class Vector3 {
	float x, y, z;
public:
	Vector3() : x(0), y(0), z(0) {}
	Vector3(float n) : x(n), y(n), z(n) {}
	Vector3(float x, float y, float z) : x(x), y(y), z(z) {}
	Vector3 operator+(const Vector3& other);
	friend ostream &operator<<(ostream &out, Vector3 v);
};
Vector3 Vector3::operator+(const Vector3&  other) {
	return Vector3( this->x + other.x, this->y + other.y, this->z + other.z);
}
ostream &operator<<(ostream &out, Vector3 v) {
	out << "<";
	out << v.x <<", ";
	out << v.y <<", ";
	out << v.z <<">";
    return out;
}
template <class TYPE>
TYPE Add(TYPE a, TYPE b) {
	TYPE result = a + b;
	return result;
}

int main(int argc, char** argv) {
	cout << Add<int>(5, 2) << endl;
	cout << Add<float>(5.5, 2.5) <<endl;
	cout << Add<double>(1.2, 2.5) << endl;
	cout << Add<string>("Cho", "colate") << endl;
	cout << Add<Vector3>(Vector3(2, 3, 8), Vector3(5, 2, 8)) << endl;
	cin.get();
	return 0;
}