#include <windows.h>

LRESULT CALLBACK WinProc(HWND hWnd,UINT message,WPARAM wParam,LPARAM lParam);

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nShowCmd) {
	//Registrar una clase de ventana
	WNDCLASSEX wc;
	ZeroMemory(&wc, sizeof(WNDCLASSEX));
	
	wc.cbSize        = sizeof(WNDCLASSEX);
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = (WNDPROC)WinProc;
	wc.cbClsExtra    = NULL;
	wc.cbWndExtra    = NULL;
	wc.hInstance     = hInstance;
	wc.hIcon         = NULL;
	wc.hCursor       = LoadCursor(NULL,IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = "Window Class";
	wc.hIconSm       = NULL;

	if(!RegisterClassEx(&wc)) {
		int error = GetLastError();
		MessageBox(NULL, "Window class creation failed", "Window Class Failed",	MB_ICONERROR);
		return error;
	}

	//Crear Ventana
	HWND hWnd = CreateWindowEx(NULL,
		"Window Class",
		"Window Title",
		WS_OVERLAPPEDWINDOW,
		0, 
		0,
		500,
		600,
		NULL,
		NULL,
		hInstance,
		NULL);

	if(!hWnd) {
		int error = GetLastError();
		MessageBox(NULL, "Window creation failed", "Window Creation Failed", MB_ICONERROR);
		return error;
	}

	//Mostrar Ventanta
	ShowWindow(hWnd, nShowCmd);

	//Event Loop: Callback Win Procedure
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));
	BOOL bRet;
	while( bRet = GetMessage(&msg, NULL, 0, 0) != 0) {
		if (bRet == -1) {
			// handle the error
			return -1;
		}
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	
	return msg.wParam;
}

LRESULT CALLBACK WinProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch(msg) {
		case WM_DESTROY: {
			PostQuitMessage(0);
			return 0;
		}
		break;
		case WM_KEYDOWN: {
			int virtualKeyCode = (int) wParam;
			if (virtualKeyCode == VK_ESCAPE) {
				PostQuitMessage(0);
				return 0;
			}
		}
		break;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}