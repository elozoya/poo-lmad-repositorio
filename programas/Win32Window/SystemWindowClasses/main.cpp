#include <windows.h>

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nShowCmd) {

	//Reference: http://msdn.microsoft.com/en-us/library/windows/desktop/ms633574%28v=vs.85%29.aspx
	char* systemClasses[] = {"Button", "ComboBox", "Edit", "ListBox", "ScrollBar", "Static"};
	//Crear Ventana
	DWORD style = WS_POPUP | WS_THICKFRAME;
	HWND hWnd = CreateWindowEx(NULL, systemClasses[0], "Press Esc to Exit", style, 200, 50, 500, 600, NULL, NULL, hInstance, NULL);
	if(!hWnd) {
		int error = GetLastError();
		MessageBox(NULL, "Window creation failed", "Window Creation Failed", MB_ICONERROR);
		return error;
	}

	//Mostrar Ventanta
	ShowWindow(hWnd, nShowCmd);
	UpdateWindow(hWnd);

	//Event Loop: Callback Win Procedure
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));
	BOOL bRet;
	while( bRet = GetMessage(&msg, NULL, 0, 0) != 0) {
		if (bRet == -1) {
			// handle the error
			return -1;
		}
		TranslateMessage(&msg);
		DispatchMessage(&msg);
		if (msg.message == WM_KEYDOWN && msg.wParam == VK_ESCAPE) {
			PostQuitMessage(0);
		}
	}
	
	return msg.wParam;
}